function removeParam(t, e) {
    var o, a = e.split("?")[0],
        r = [],
        i = -1 !== e.indexOf("?") ? e.split("?")[1] : "";
    if ("" !== i) {
        r = i.split("&");
        for (var l = r.length - 1; l >= 0; l -= 1) o = r[l].split("=")[0], o === t && r.splice(l, 1);
        a = a + "?" + r.join("&")
    }
    return a
}

function isMobile() {
    return !!(navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i))
}
var folha = folha || {},
    artefolha = artefolha || {};
artefolha.barra = {}, artefolha.ads = {
    isAds: !1
};
var loadScript = function(t, e) {
        var o = document.createElement("script");
        o.type = "text/javascript", o.readyState ? o.onreadystatechange = function() {
            "loaded" !== o.readyState && "complete" !== o.readyState || (o.onreadystatechange = null, e())
        } : o.onload = function() {
            e()
        }, o.src = t, document.body.appendChild(o)
    },
    head = document.head || document.getElementsByTagName("head")[0],
    style = document.createElement("style"),
    scriptBarraUol = document.createElement("script"),
    header = document.createElement("header"),
    footer, foot, url = window.location.href + "?";
artefolha.barra = {
    barraUol: function() {
        var t = {
                cl: "barraparceiro",
                pv: "barraparceiro-pv"
            },
            e = "barrauol";
        window.location.href.indexOf("folha.uol.com.br") > -1 && (e += " barrafolha");
        var o = '@font-face{src:url(https://stc.uol.com/c/webfont/projeto-grafico/v2/icones-setas/uol-icones-setas.eot?20141113);src:url(https://stc.uol.com/c/webfont/projeto-grafico/v2/icones-setas/uol-icones-setas.eot?20141113#iefix) format("embedded-opentype"), url(https://stc.uol.com/c/webfont/projeto-grafico/v2/icones-setas/uol-icones-setas.woff?20141113) format("woff"), url(https://stc.uol.com/c/webfont/projeto-grafico/v2/icones-setas/uol-icones-setas.ttf?20141113) format("truetype"), url(https://stc.uol.com/c/webfont/projeto-grafico/v2/icones-setas/uol-icones-setas.svg?20141113#uol-icones-e-setas) format("svg");font-family:UOLIcons;font-weight:400;font-style:normal;}@font-face{src:url(https://stc.uol.com/c/webfont/projeto-grafico/uol-font/uol-text-regular.eot?v5);src:local("?"), url(https://stc.uol.com/c/webfont/projeto-grafico/uol-font/uol-text-regular.eot?#iefix) format("embedded-opentype"), url(https://stc.uol.com/c/webfont/projeto-grafico/uol-font/uol-text-regular.woff?v5) format("woff"), url(https://stc.uol.com/c/webfont/projeto-grafico/uol-font/uol-text-regular.ttf?v5) format("truetype"), url(https://stc.uol.com/c/webfont/projeto-grafico/uol-font/uol-text-regular.svg?v5#uol-text-regular) format("svg");font-family:UOLText;font-weight:400;font-style:normal;}@font-face{src:url(https://stc.uol.com/c/webfont/projeto-grafico/uol-font/uol-text-light.eot?v5);src:local("?"), url(https://stc.uol.com/c/webfont/projeto-grafico/uol-font/uol-text-light.eot?#iefix) format("embedded-opentype"), url(https://stc.uol.com/c/webfont/projeto-grafico/uol-font/uol-text-light.woff?v5) format("woff"), url(https://stc.uol.com/c/webfont/projeto-grafico/uol-font/uol-text-light.ttf?v5) format("truetype"), url(https://stc.uol.com/c/webfont/projeto-grafico/uol-font/uol-text-light.svg?v5#uol-text-light) format("svg");font-family:UOLTextLight;font-style:normal;}#barrauol{background-color:#262626;color:#FFF;font:14px UOLTextLight,sans-serif,Helvetica,Arial;height:44px;margin:0;max-height:44px;padding:0;position:relative;text-align:center;width:100%;z-index:9;}#barrauol .containerUOL{display:inline-table;width:1170px;}#barrauol .al-left{float:left;}#barrauol .al-right{float:right;}#barrauol .al-center{height:44px;left:50%;margin-left:-120px;position:absolute;text-align:center;width:100px;}#barrauol .fs-15{font-size:15px;}#barrauol a{text-decoration:none;}#barrauol a,#barrauol a:focus,#barrauol a:hover,#barrauol a:visited{color:#FFF;}#barrauol a,#barrauol nav li{transition:all .1s linear;}#barrauol ul,#barrauol li{list-style:none;margin:0;padding:0;}#barrauol nav{background:#262626;height:44px;position:relative;width:auto;}#barrauol nav a{display:inline-block;font:14px UOLText, sans-serif, Helvetica, Arial;height:44px;line-height:44px;padding:0 10px;vertical-align:middle;letter-spacing:normal;}#barrauol nav i{display:inline-block;font-style:normal;margin:-3px 5px 0 0;vertical-align:middle;}#barrauol nav a:before{display:none;}#barrauol nav li{float:left;height:44px;background:transparent;border:0;}#barrauol nav li.first a{padding-left:15px;}#barrauol nav li.last a{padding-right:15px;}#barrauol nav li:hover{background-color:#404040;}#barrauol .uolicons{font-family:UOLIcons;color:#fff;}#barrauol i.icon-email{font-size:24px;width:25px;}#barrauol i:before{font-family:UOLIcons;margin:-3px 5px 0 0;opacity:1;}#barrauol i.icon-email:before{content:"\\e626";font-size:26px;width:24px;}#barrauol i.icon-batepapo{font-size:26px;margin-top:1px;width:27px;}#barrauol i.icon-batepapo:before{content:"\\e654";font-size:26px;width:30px;}#barrauol i.icon-search{font-size:20px;margin-top:-4px;width:18px;}#barrauol i.icon-search:before{content:"\\e619";font-size:22px;width:20px;}#barrauol .logouol{display:inline-block;font:48px UOLIcons;height:44px;line-height:40px;margin-left:-8px;position:relative;width:70px;}#barrauol .logouol:hover{opacity:.9;}#barrauol .logouol:before{content:"\\e636";display:inline-block;padding-left:30px;padding-top:1px;}#barrauol .logouol span{padding-left:30px;}#barrauol .logouol img{border:none;}#barrauol .logouol img{height:26px;left:0;position:absolute;top:8px;width:26px;}#barrauol .logouol img.uol_aniversario{left:75px;width:90px;}@media (max-width:1024px){#barrauol nav a{font-family:UOLText;font-size:11px;}#barrauol .containerUOL{width:940px;}}@media (max-width:768px){#barrauol .al-center{margin-left:-90px;}#barrauol nav{display:none;}#barrauol .containerUOL{display:block;width:auto;}}/* PERSONALIZAÃƒâ€¡ÃƒÆ’O DA ALTURA PARA BARRA DA FOLHA */#barrauol.barrafolha,#barrauol.barrafolha .al-center,#barrauol.barrafolha nav,#barrauol.barrafolha div,#barrauol.barrafolha nav li,#barrauol.barrafolha a,#barrauol.barrafolha .logouol{height:40px;max-height:40px;}#barrauol.barrafolha .logouol,#barrauol.barrafolha a{line-height:40px;}#barrauol.barrafolha .al-right li.assine_sac{display:none;}',
            a = '<div class="containerUOL"> <nav class="al-left"> <ul> <li class="item-batepapo"><a href="http://click.uol.com.br/?rf={{ref-cl}}&u=http://batepapo.uol.com.br/"><i class="uolicons icon-batepapo"></i>BATE-PAPO</a></li> <li><a href="http://click.uol.com.br/?rf={{ref-cl}}&u=http://email.uol.com.br/"><i class="uolicons icon-email"></i>E-MAIL</a></li> <li><a href="http://click.uol.com.br/?rf={{ref-cl}}&u=http://busca.uol.com.br/"><i class="uolicons icon-search"></i>BUSCA</a></li> </ul> </nav> <div class="al-center"> <a class="logouol" href="http://click.uol.com.br/?rf={{ref-cl}}&u=http://www.uol.com.br/"><img src="https://imguol.com/c/_layout/v1/_geral/icones/logo-uol-52x52.png" alt="UOL - O melhor conte&uacute;do" title="UOL - O melhor conte&uacute;do"><img class="uol_aniversario" src="https://imguol.com/c/_layout/v1/_geral/icones/20anos.svg" alt="UOL - O melhor conte&uacute;do" title="UOL - O melhor conte&uacute;do"></a> </div> <nav class="al-right"> <ul> <li><a href="http://click.uol.com.br/?rf={{ref-cl}}&u=http://clicklogger.rm.uol.com.br/?prd=16&grp=src:13;chn:102;creative:barrauol_parceiros;thm:uol_host&msr=Cliques%20de%20Origem:1&oper=11&redir=http://www.uolhost.uol.com.br/?cmpid=barrauol-parceiros">UOL HOST</a></li> <li><a href="http://click.uol.com.br/?rf={{ref-cl}}&u=https://clicklogger.rm.uol.com.br/?prd=32&grp=src:13;chn:102;creative:barrauol_parceiros;thm:pagseguro&msr=Cliques%20de%20Origem:1&oper=11&redir=https://pagseguro.uol.com.br/?cmpid=barrauol-parceiros">PAGSEGURO</a></li> <li><a href="http://click.uol.com.br/?rf={{ref-cl}}&u=http://clicklogger.rm.uol.com.br/?prd=78&grp=src:13;chn:102;creative:barrauol_parceiros;thm:cursos_online&msr=Cliques%20de%20Origem:1&oper=11&redir=http://cursosonline.uol.com.br/assinatura/cursoslivres/?cmpid=barrauol-parceiros">CURSOS ONLINE</a></li> <li class="assine_sac"><a href="http://click.uol.com.br/?rf={{ref-cl}}&u=http://clicklogger.rm.uol.com.br/?prd=11&grp=src:13;chn:102;creative:barrauol_parceiros;thm:assine&msr=Cliques%20de%20Origem:1&oper=11&redir=http://assine.uol.com.br/?cmpid=barrauol-parceiros">ASSINE / SAC</a></li> </ul> </nav> </div> '.replace(/{{ref-cl}}/gi, t.cl).replace(/{{ref-pv}}/gi, t.pv);
        return a = a.replace(/\{\{ref-cl\}\}/gi, t.cl).replace(/\{\{ref-pv\}\}/gi, t.pv), window.rewriteBarraUOL = function() {
            if (!document.getElementById("css-barrauol")) {
                var t = document.createElement("style");
                t.type = "text/css", t.id = "css-barrauol";
                var r = o;
                t.styleSheet ? t.styleSheet.cssText = r : t.appendChild(document.createTextNode(r)), document.getElementsByTagName("head")[0].appendChild(t)
            }
            var i = document.getElementById("barrauol");
            i ? (i.className = e, i.innerHTML = a) : console.error("[barra uol] rewriteBarraUOL() exige um div#barrauol")
        }, "<style>" + o + '</style><div class="' + e + '" id="barrauol">' + a + "</div>"
    },
    writeBarra: function() {
        var t = isMobile() ? "//m.folha.uol.com.br/" : "//www.folha.uol.com.br/",
            e = artefolha.barra.barraUol() + '<div class="barra"><a href="' + t + '"><img src="http://f.i.uol.com.br/app/2/logo_folha_estrelas.svg" class="logo" alt="Folha de S.Paulo" /></a><a href="http://assine.folha.com.br/folha/assinatura/?gid=FOL" target="_blank" class="assine-fol">Assine a <b>Folha</b></a></div>';
        header.id = "topofolha", header["class"] = "topo", header.innerHTML = e, document.body.insertBefore(header, document.body.firstChild)
    },
    writeSocial: function() {
        function t(t) {
            this.parentNode.classList.toggle("opened")
        }
        if (fb = '<a href="https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(url + "cmpid=compfb") + '" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50"><path fill="#3C5998" d="M27.157 14.542c-1.157 0-2.208.625-2.208 2.255v3.863h5.408l-.724 5.427h-4.686v14.016h-5.586V26.086h-4.757V20.66h4.757v-3.324c0-5.277 2.483-7.957 6.937-7.957 2.216 0 4.322.246 4.322.246v4.917h-3.465z"/></svg></a>', twitter = '<a href="https://twitter.com/home?status=' + encodeURIComponent("Veja " + document.title.split(" - ")[0] + " " + url + "cmpid=comptw via @folha") + '" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50"><path fill="#35CCFF" d="M38.065 18.834v.792c0 2.855-.66 5.604-1.98 8.242-1.318 2.66-3.36 4.89-6.132 6.69-2.79 1.83-5.978 2.74-9.56 2.74-3.495 0-6.683-.924-9.562-2.772.396.046.902.066 1.518.066 2.88 0 5.452-.878 7.716-2.638-1.408-.044-2.633-.473-3.676-1.288-1.045-.81-1.754-1.833-2.127-3.064.2.088.572.13 1.123.13.57 0 1.12-.063 1.647-.194-1.384-.287-2.555-.996-3.51-2.13-.957-1.13-1.435-2.464-1.435-4.003v-.066c.792.44 1.714.703 2.77.79-1.846-1.273-2.77-3.01-2.77-5.21 0-1.03.286-2.064.858-3.097 3.406 4.154 7.692 6.308 12.86 6.462-.134-.395-.2-.858-.2-1.385 0-1.713.61-3.175 1.83-4.385 1.22-1.208 2.698-1.813 4.435-1.813 1.803 0 3.298.64 4.485 1.912 1.274-.264 2.592-.747 3.957-1.45-.418 1.45-1.317 2.593-2.704 3.43 1.208-.177 2.394-.507 3.562-.99-.816 1.23-1.85 2.307-3.103 3.23z"/></svg></a>', gplus = '<a href="https://plus.google.com/share?url=' + url + 'compgp" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50"><path fill="#DD4B39" d="M42.05 23.007v-3.74h-2.743v3.736h-3.99v2.745h3.736v3.74h2.745V25.75h3.99v-2.743H42.05zm-20.2-.25v4.237h5.737c-.5 2.497-2.745 4.49-5.736 4.49-3.49 0-6.482-2.99-6.482-6.48s2.992-6.486 6.483-6.486c1.5 0 2.992.498 3.99 1.5l3.242-3.244c-1.995-1.5-4.24-2.745-7.23-2.745-5.983 0-10.973 4.986-10.973 10.97 0 5.988 4.987 10.973 10.97 10.973 6.233 0 10.474-4.486 10.474-10.723 0-.747 0-1.746-.254-2.492H21.85z"/></svg></a>', whatsapp = '<a href="whatsapp://send?text=' + encodeURIComponent("Veja " + document.title.split(" - ")[0] + " - " + url + "cmpid=compw via Folha de S.Paulo") + '"><svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50"><path fill="#43D854" d="M36.41 13.714C33.355 10.66 29.34 9 25.053 9 16.27 9 9.09 16.178 9.09 25.016c0 2.84.75 5.572 2.144 7.982L8.93 41.3l8.463-2.25c2.357 1.286 4.982 1.93 7.66 1.93 8.838.054 16.016-7.125 16.016-15.963 0-4.286-1.66-8.304-4.66-11.303zM25.053 38.3c-2.41 0-4.714-.643-6.75-1.874l-.482-.268-5.035 1.34 1.34-4.93-.323-.48c-1.34-2.144-2.036-4.555-2.036-7.072 0-7.338 5.946-13.338 13.285-13.338 3.535 0 6.91 1.393 9.428 3.91 2.52 2.518 3.91 5.892 3.91 9.428.002 7.338-5.998 13.285-13.336 13.285zm7.285-9.963c-.376-.213-2.358-1.178-2.733-1.284-.375-.108-.643-.215-.91.213-.268.376-1.018 1.286-1.286 1.554-.213.268-.48.32-.855.107-.375-.214-1.714-.644-3.214-1.982-1.177-1.07-1.98-2.356-2.25-2.785-.213-.375 0-.643.162-.804.16-.16.375-.482.59-.696.213-.214.267-.375.374-.643.107-.268.054-.482-.053-.696-.107-.213-.91-2.142-1.232-2.945-.32-.804-.642-.696-.91-.696h-.75s-.695.106-1.07.48-1.393 1.394-1.393 3.322c0 1.93 1.446 3.857 1.607 4.125.214.268 2.84 4.285 6.856 6.054.965.43 1.715.643 2.304.857.964.32 1.82.268 2.518.16.75-.106 2.357-.964 2.678-1.874.32-.91.32-1.715.215-1.875.05-.27-.218-.376-.646-.59z"/></svg></a>', isMobile()) {
            var e = document.createElement("div");
            e.id = "fol-lateral-share", e.innerHTML = '            <div class="share-btn" id="fol-main-share">                <p><svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50"><path d="M36.577 35.933c0 2.987-2.405 5.467-5.467 5.467-2.988 0-5.466-2.48-5.466-5.467 0-.364.072-.656.072-.947L16.242 29.3c-.947.73-2.114 1.166-3.354 1.166-2.987 0-5.465-2.478-5.465-5.466 0-3.06 2.478-5.465 5.465-5.465 1.24 0 2.406.363 3.354 1.093l9.475-5.686c0-.292-.074-.583-.074-.875 0-3.062 2.48-5.466 5.467-5.466 3.062 0 5.466 2.405 5.466 5.467 0 2.99-2.403 5.467-5.466 5.467-1.238 0-2.404-.438-3.28-1.167l-9.547 5.684c.072.293.072.584.072.948 0 .292 0 .583-.072.874l9.547 5.688c.876-.73 2.042-1.094 3.28-1.094 3.063 0 5.466 2.406 5.467 5.467z"/></svg></div><div class="share-btn fol-facebook">' + fb + '</div><div class="share-btn fol-whatsapp">' + whatsapp + '</div><div class="share-btn fol-twitter">' + twitter + "</div>", document.body.insertBefore(e, document.body.childNodes[0])
        } else {
            document.documentElement.classList.add("not-mobile");
            var e = document.createElement("p");
            e.innerHTML = '<div id="fol-lateral-share" class="opened">                <div class="share-btn" id="fol-main-share">                    <p><svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50"><path d="M36.577 35.933c0 2.987-2.405 5.467-5.467 5.467-2.988 0-5.466-2.48-5.466-5.467 0-.364.072-.656.072-.947L16.242 29.3c-.947.73-2.114 1.166-3.354 1.166-2.987 0-5.465-2.478-5.465-5.466 0-3.06 2.478-5.465 5.465-5.465 1.24 0 2.406.363 3.354 1.093l9.475-5.686c0-.292-.074-.583-.074-.875 0-3.062 2.48-5.466 5.467-5.466 3.062 0 5.466 2.405 5.466 5.467 0 2.99-2.403 5.467-5.466 5.467-1.238 0-2.404-.438-3.28-1.167l-9.547 5.684c.072.293.072.584.072.948 0 .292 0 .583-.072.874l9.547 5.688c.876-.73 2.042-1.094 3.28-1.094 3.063 0 5.466 2.406 5.467 5.467z"/></svg></div><div class="share-btn fol-facebook">' + fb + '</div><div class="share-btn fol-twitter">' + twitter + '</div><div class="share-btn fol-gplus">' + gplus + "</div></div>", document.body.insertBefore(e, document.body.childNodes[0])
        }
        var o = document.getElementById("fol-main-share");
        o.addEventListener("click", t, !1), artefolha.barra.writeStats()
    },
    writeWall: function() {
        var t = document.createElement("script");
        t.type = "text/javascript", t.src = "http://www1.folha.uol.com.br/folha/furniture/paywall/loader.js?t=20150311", t.async = !1, t.defer = !0, $.getScript("http://www1.folha.uol.com.br/folha/furniture/paywall/loader.js?t=20150311", function() {
            $("body").append('<script src="http://paywall.folha.uol.com.br/' + (paywall.isTest() ? "block" : "wall") + ".jsonp?callback=paywall.inicio&url=" + paywall.url() + "&referer=" + paywall.referrer() + "&clear=" + encodeURI(new Date) + '" async></script>')
        })
    },
    writeAudience: function() {
        artefolha.barra.writeSocial()
    },
    writeStats: function() {
        var t = {
            url: "http://stats1.folha.uol.com.br/stats",
            old_url: "http://stats.folha.uol.com.br/stats",
            random: function(t) {
                var e, o;
                return t = t || 10, e = Math.pow(10, t - 1), o = Math.pow(10, t) - 1, Math.round(Math.random() * (o - e) + e)
            },
            init: function() {
                var t = this,
                    e = [location.protocol, "//", location.host, location.pathname].join("");
                $("body").append('<img src="' + t.url + "?url=" + escape(e) + "&amp;ref=" + escape(document.referrer || "") + "&amp;rand=" + t.random() + '" width="1" height="1" alt="" style="display:none;">')
            }
        };
        $(document).ready(function() {
            t.init()
        })
    },
    writeStyle: function() {
        var t = 'body{padding-top:84px;transition:all .2s;-webkit-transition:all .2s;-moz-transition:all .2s;-o-transition:all .2s;-ms-transition:all .2s}#topofolha{margin:0;width:100%;height:84px;background:#eaeff2;position:fixed;top:0;left:0;z-index:10000;font-family:HelveticaNeue,"Helvetica Neue",Helvetica,Arial,sans-serif;box-shadow:0 0 8px rgba(0,0,0,.3);-webkit-font-smoothing:antialiased;-moz-ios-font-smoothing:greyscale;transition:all .2s;-webkit-transition:all .2s;-moz-transition:all .2s;-o-transition:all .2s;-ms-transition:all .2s}#topofolha .barra{max-width:1170px; margin:0 auto; position:relative;}#topofolha .logo{height:33px;margin:7px auto;display:block;position:relative}#topofolha .logo img{height:100%}#topofolha .assine-fol{position:absolute;right:0;padding:0 10px;line-height:40px;top:-7px;color:#7a8080;text-decoration:none;transition:color .15s;-webkit-transition:color .15s;-moz-transition:color .15s;-o-transition:color .15s;-ms-transition:color .15s}#topofolha .assine-fol b{transition:color .15s;-webkit-transition:color .15s;-moz-transition:color .15s;-o-transition:color .15s;-ms-transition:color .15s}#topofolha .assine-fol b{font-weight:700}#topofolha .assine-fol:hover,#topofolha .assine-fol:hover b{color:#00adef}#fol-lateral-share *{margin:0;padding:0}#fol-lateral-share{z-index:1000;position:fixed;right:3%;bottom:10px;height:50px;width:50px}#fol-lateral-share .share-btn{transform-origin:50% 50%;-webkit-transform-origin:50% 50%;-moz-transform-origin:50% 50%;-o-transform-origin:50% 50%;-ms-transform-origin:50% 50%;box-sizing:border-box;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;width:50px;height:50px;border-radius:50%;border:4px solid;position:absolute;z-index:0;background-color:#fff;cursor:pointer;transform:scale(.8);-webkit-transform:scale(.8);-moz-transform:scale(.8);-o-transform:scale(.8);-ms-transform:scale(.8);transition:transform .3s;-webkit-transition:transform .3s;-moz-transition:transform .3s;-o-transition:transform .3s;-ms-transition:transform .3s}#fol-lateral-share a,#fol-main-share p{display:block;text-decoration:none;position:relative;width:100%;height:100%;text-align:center;line-height:60px;-webkit-margin-before:0;-webkit-margin-after:0}#fol-lateral-share svg{width:100%;height:100%;/*transform:translate(50%,50%);left:-20%;top:-20%;position:absolute*/}#main-share p{margin:-3px 0 0 -2px}.mobile #fol-main-share path{fill:#FFF}.not-mobile #fol-main-share path{fill:#fff;transition:fill .15s;-webkit-transition:fill .15s;-moz-transition:fill .15s;-o-transition:fill .15s;-ms-transition:fill .15s}.not-mobile #fol-main-share{transition:all .15s;-webkit-transition:all .15s;-moz-transition:all .15s;-o-transition:all .15s;-ms-transition:all .15s}.not-mobile #fol-main-share:hover path{fill:#fff}#fol-lateral-share #fol-main-share{background-color:#00adef;border-color:#00adef;z-index:1;transform:scale(1);-webkit-transform:scale(1);-moz-transform:scale(1);-o-transform:scale(1);-ms-transform:scale(1)}.not-mobile #fol-lateral-share #fol-main-share:hover{border-color:#666;background-color:#666}#fol-lateral-share .fol-facebook{border-color:#3c5998}#fol-lateral-share.opened .share-btn.fol-facebook{transform:translateY(-55px) scale(1);-webkit-transform:translateY(-55px) scale(1);-moz-transform:translateY(-55px) scale(1);-o-transform:translateY(-55px) scale(1);-ms-transform:translateY(-55px) scale(1)}#fol-lateral-share .fol-twitter{border-color:#35CCFF}.not-mobile #fol-lateral-share.opened .share-btn.fol-twitter{transform:translateY(-110px) scale(1);-webkit-transform:translateY(-110px) scale(1);-moz-transform:translateY(-110px) scale(1);-o-transform:translateY(-110px) scale(1);-ms-transform:translateY(-110px) scale(1)}.mobile #fol-lateral-share.opened .share-btn.fol-twitter{transform:translateY(-165px) scale(1);-webkit-transform:translateY(-165px) scale(1);-moz-transform:translateY(-165px) scale(1);-o-transform:translateY(-165px) scale(1);-ms-transform:translateY(-165px) scale(1)}#fol-lateral-share .fol-gplus{border-color:#DD4B39}#fol-lateral-share.opened .share-btn.fol-gplus{transform:translateY(-165px) scale(1);-webkit-transform:translateY(-165px) scale(1);-moz-transform:translateY(-165px) scale(1);-o-transform:translateY(-165px) scale(1);-ms-transform:translateY(-165px) scale(1)}#fol-lateral-share .fol-whatsapp{border-color:#43D854}#fol-lateral-share.opened .share-btn.fol-whatsapp{transform:translateY(-110px) scale(1);-webkit-transform:translateY(-110px) scale(1);-moz-transform:translateY(-110px) scale(1);-o-transform:translateY(-110px) scale(1);-ms-transform:translateY(-110px) scale(1)}@media only screen and (max-width:700px){.assine-fol{right:auto;left:10px}}@media only screen and (max-width:490px){.assine-fol{display:none}}';
        style.type = "text/css", style.styleSheet ? style.styleSheet.cssText = t : style.appendChild(document.createTextNode(t)), head.appendChild(style), artefolha.barra.writeBarra(), artefolha.barra.writeAudience()
    },
    init: function() {
        isMobile() ? document.documentElement.classList.add("mobile") : document.documentElement.classList.add("not-mobile"), artefolha.barra.writeStyle(), artefolha.barra.loadFolhaScript()
    },
    loadFolhaScript: function() {}
}, document.addEventListener("DOMContentLoaded", function() {
    "interactive" !== document.readyState && "complete" !== document.readyState || ("undefined" == typeof jQuery && "undefined" == typeof $ ? loadScript("http://arte.folha.uol.com.br/library/jquery/1.11.1/jquery.min.js", function() {
        artefolha.barra.init()
    }) : artefolha.barra.init())
});
var urlPath = window.location.pathname,
    desk = urlPath.split("/")[1];
window.folha = window.folha || {}, folha.omniture = folha.omniture || {
    info: {
        channel: desk,
        subchannel: ["infograficos"],
        type: "infograficos",
        title: encodeURI(document.title.split(" - ")[0]),
        special: {
            title: "",
            type: ""
        }
    }
};
var omtrScript = document.createElement("script");
omtrScript.setAttribute("src", "http://me.jsuol.com/omtr/folhacom.js"), omtrScript.setAttribute("language", "javascript"), omtrScript.setAttribute("type", "text/javascript"), document.body.appendChild(omtrScript), window.ivcUrl = ("https:" == document.location.protocol ? "https://datpdpmej1fn2.cloudfront.net/" : "http://ivccftag.ivcbrasil.org.br/") + "ivc.js",
    function(t, e, o, a, r, i, l) {
        t[r] || (t.GlobalIvcNamespace = t.GlobalIvcNamespace || [], t.GlobalIvcNamespace.push(r), t[r] = function() {
            (t[r].q = t[r].q || []).push(arguments)
        }, t[r].q = t[r].q || [], i = e.createElement(o), l = e.getElementsByTagName(o)[0], i.async = 1, i.src = a, l.parentNode.insertBefore(i, l))
    }(window, document, "script", ivcUrl, "ivc"), window.ivc("newTracker", "cf", "ivccf.ivcbrasil.org.br", {
        idWeb: "125"
    }), window.ivc("trackPageView");
var nvgrtTag = document.createElement("img");
nvgrtTag.setAttribute("src", "//navdmp.com/req?acc=23947&amp;cus=101725&amp;/img=1"), nvgrtTag.setAttribute("id", "nvg_rt"), nvgrtTag.setAttribute("alt", "Pixel tag"), nvgrtTag.setAttribute("width", "1"), nvgrtTag.setAttribute("height", "1"), nvgrtTag.style.display = "none", document.body.appendChild(nvgrtTag), window._sf_async_config = {}, window._sf_async_config.uid = 50059, window._sf_async_config.domain = "folha.com.br", window._sf_async_config.useCanonical = !0, window._sf_async_config.sections = "Home", window._sf_async_config.authors = "Folha",
    function() {
        function t() {
            window._sf_endpt = (new Date).getTime();
            var t = document.createElement("script");
            t.setAttribute("language", "javascript"), t.setAttribute("type", "text/javascript"), t.setAttribute("src", "//static.chartbeat.com/js/chartbeat.js"), document.body.appendChild(t)
        }
        var e = window.onload;
        window.onload = "function" != typeof window.onload ? t : function() {
            e(), t()
        }
    }(), window.clickCount = function(t) {
        var e = "";
        omtrHitCounter("infograficos", "item " + e), window.ivc("trackPageView")
    };	