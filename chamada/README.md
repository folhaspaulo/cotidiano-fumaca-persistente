# Chamada HTML

## Para quando a imagem Spiffy não é o bastante

Para editar: abra o arquivo ```chamada.html``` e altere as linhas ```11``` 1 ```13```. Mude o link do ```<a>``` e a descrição da imagem.

```
<a href="http://arte.folha.uol.com.br/" target="_blank">
    <img src="chamada.png" alt="Descrição da imagem" style="width:100%;" />
</a>
```

Altere a imagem usando o ```template.psd``` e salve-a como ```chamada.png```
